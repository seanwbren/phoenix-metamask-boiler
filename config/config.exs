# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :myapp,
  ecto_repos: [MyApp.Repo]

# Configures the endpoint
config :myapp, MyApp.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Jo3lTgor7Bc5RqeUf042hXOuWrfpmIypdWXYjlr2zc/0ZRsOGCJgnxOVTtrY7TGL",
  render_errors: [view: MyApp.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: MyApp.PubSub,
           adapter: Phoenix.PubSub.PG2]

#cronjob scheduler
config :myapp, MyApp.Scheduler,
  global: true,
  overlap: true,
  jobs: [
    # Every 2 minutes check DB for finished games and resolve bets
    # "*/2 * * * *": : {myapp.Transaction, :resolve_bets}
    # Every 6 minutes update the conversion prices for Eth
     {"*/6 * * * *", {MyApp.Util, :putCurrencyConversionsInCache, []}}
  ]


# Configures Elixir's Logger
config :logger, :console,
  format: "$date $time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Import Timber, structured logging.  Really good and free to start, I recommend
# import_config "timber.exs"
