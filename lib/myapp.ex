defmodule MyApp do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # this cache should be updated every 6 minutes through Quantum calling CoinMarketCap Api
      worker(ConCache, [[ttl_check: :timer.hours(1), ttl: :timer.hours(24)], [name: :ethconversionprices]], id: :ethfiatconversionprice),    
      worker(MyApp.Scheduler, []),       
      supervisor(MyApp.Repo, []),
      supervisor(MyApp.Web.Endpoint, []),
      supervisor(Task.Supervisor, [[name: MyApp.TaskSupervisor]]),
      # Start your own worker by calling: MyAppWorker.start_link(arg1, arg2, arg3)
      # worker(MyApp.Worker, [arg1, arg2, arg3]),
    ]
     

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MyApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

end
