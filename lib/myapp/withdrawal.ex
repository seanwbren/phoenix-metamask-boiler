defmodule MyApp.Withdrawal do
  use Ecto.Schema
  import Ecto.Changeset
  alias MyApp.Withdrawal


  schema "withdrawals" do
    field :destination_address, :string
    field :amount, :decimal, precision: 26 # in Wei
    field :signature_result, :string
    field :tx_status, :string, default: "pending" # will be "pending" , "failed" , or "success"
    field :tx_hash, :string

    belongs_to :wallet, MyApp.Wallet, references: :address, foreign_key: :address, primary_key: true, type: :string
    

    timestamps()
  end


  @doc false
  def changeset(%Withdrawal{} = withdrawal, attrs) do
    withdrawal
    |> cast(attrs, [:destination_address, :amount, :signature_result, :tx_status, :tx_hash])
    |> validate_required([:destination_address, :amount, :signature_result])
    end
end
