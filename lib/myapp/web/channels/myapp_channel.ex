defmodule MyApp.Web.MyAppChannel do
  use MyApp.Web, :channel
  alias MyApp.Web.LayoutView
  alias Phoenix.View

  def join("myapp:users", _params, socket) do
    :timer.send_interval(2000, :checkForNewWallet)
    socket = assign(socket, :metamaskbool, false)
    send(self, {:after_join})
    {:ok, socket}
  end

  # 2 second ping checks for current Metamask wallet and will reassign params["walletAddress"]
  def handle_info(:checkForNewWallet, socket) do
    push socket, "checkForNewWallet", %{msg: nil}
    {:noreply, socket}
  end

  def handle_info({:after_join}, socket) do
    push socket, "user_alert", %{
      message: "Welcome to MyApp. Try out the Deposit database storage through Ecto/Postgres, and see what happens in the 3 different states of MetaMask not being installed, wallet not connected, and wallet connected."
    }
    {:noreply, socket}
    end


  def handle_info(_msg, state) do
  {:noreply, state}
  end

end