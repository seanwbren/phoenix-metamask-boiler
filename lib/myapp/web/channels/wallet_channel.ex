defmodule MyApp.Web.WalletChannel do
  use MyApp.Web, :channel
  import Ecto
  require Logger
  alias MyApp.{Wallet, Repo, Deposit, Withdrawal, Util}

  # params are {"walletAddress": activeWallet, "networkName": networkName}
  def join("wallet:" <> wallet_address, params, socket) do
    socket = assign(socket, :networkName, params["networkName"])
    send(self, {:after_join, wallet_address})
    {:ok, assign(socket, :walletAddress, wallet_address)} 
  end

  def handle_info({:after_join, wallet_address}, socket) do
    Repo.insert(%Wallet{address: wallet_address}, on_conflict: :nothing) #creates or does nothing if wallet already seen
    balance = getWalletBalance(wallet_address)
    usd_price = getEthPrice()  # easy to switch to other currencies
    if !eth_price do           # should only be nil right when the app starts
      usd_price = Util.getCMCEthPrice()
      ConCache.put(:ethconversionprices, "usd", usd_price) 
    end
    push socket, "update_balance", %{
      ecto_balance: balance,
      change: nil,
      price: eth_price
    }
    {:noreply, assign(socket, :balance, balance)}
  end

  def handle_in(event, params, socket) do
    wallet = Repo.get(Wallet, socket.assigns.walletAddress)
    handle_in(event, params, wallet, socket)
  end

  def handle_in("deposit_insert", params, wallet, socket) do
    string_amount = String.replace_leading(params["amount"], ".", "0.")
    {float_int, _} = Float.parse(string_amount)
    wei_amount = round(float_int*1000000000000000000) # 18 zeroes
  
    case Repo.insert(%Deposit{address: params["account"], amount: wei_amount}) do
      {:ok, deposit} ->
        push socket, "metamask_deposit", %{
          account: params["account"],
          amount: wei_amount,
          id: deposit.id
          }
        {:reply, :ok, socket}  
      {:error, changeset} ->
        {:reply, {:error, %{errors: changeset}}, socket}
    end
  end




  # params are {address: withdrawAddress, amount: withdrawAmount}
  def handle_in("withdraw", params, wallet, socket) do
    push socket, "withdraw", %{
      address: params["address"],
      amount: params["amount"]
    }
    {:reply, :ok, socket}
  end

  # params are {account: wallet, amount: value}
  def handle_in("credit_account", params, wallet, socket) do
    address = params["account"]
    amount = params["amount"]

    # found this method on stackoverflow...seems to work
    case Repo.insert(%Wallet{address: address}, returning: true, conflict_target: :address, on_conflict: [inc: [balance: amount]]) do
      {:ok, wallet} -> 
        # IO.inspect(wallet)
        broadcast! socket, "update_balance", %{
          ecto_balance: nil,
          change: amount,  #in Wei
          price: nil
        }
        {:reply, :ok, socket}

      {:error, changeset} -> 
        {:reply, {:error, %{errors: changeset}}, socket}
      end

  end

  def handle_in("new_wallet", wallet, _payload, socket) do
    {:stop, :normal, socket}
  end
    
  def handle_info(_msg, state) do
    {:noreply, state}
    end
    
  def terminate(reason, socket) do
    Logger.debug"> leave #{inspect reason} for wallet channel #{socket.assigns[:walletAddress]}"
    :ok
  end

  defp getWalletBalance(wallet_address) do
    ecto_balance = List.first(Repo.all(from w in Wallet, where: ilike(w.address, ^wallet_address))).balance
  end

  defp getEthPrice() do
      ConCache.get(:ethconversionprices, "usd")
  end
    

end