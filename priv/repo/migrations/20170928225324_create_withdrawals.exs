defmodule MyAapp.Repo.Migrations.CreateWithdrawals do
  use Ecto.Migration

  def change do
    create table(:withdrawals) do
      add :address, references(:wallets, column: :address, on_delete: :nothing, type: :string), null: false
      add :destination_address, :string, null: false
      add :amount, :decimal, null: false
      add :signature_result, :string, null: false
      add :tx_status, :string
      add :tx_hash, :string
        
  

      timestamps()
    end

  end
end
