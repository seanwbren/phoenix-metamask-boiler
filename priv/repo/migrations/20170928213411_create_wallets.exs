defmodule MyApp.Repo.Migrations.CreateWallets do
  use Ecto.Migration

  def change do
    create table(:wallets, primary_key: false) do
      add :address, :string, primary_key: true
      add :balance, :decimal

      timestamps()
    end

    create unique_index(:wallets, [:address])

  end
end
