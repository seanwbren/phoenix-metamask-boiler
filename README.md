# myapp

To start your Phoenix app follow these steps (and you can find the Phoenix dev requirements [`here`](http://https://hexdocs.pm/phoenix/installation.html):

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * `cd ..` and start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser. Chrome with MetaMask installed and set to Kovan Network is necessary.

This is what it should look like correctly installed: 
![](/assets/static/images/intendedresult.png)

## Things to look out for:
 * The deposit address is hardcoded on line 99 of the wallet.js file.
 * Ending `mix phx.server` with ctrl+Z doesn't end the erlang process     on the port. so you can run `killall -9 "beam.smp"`.  Set up an        alias for this...
 * Right now you need to reload the page for MetaMask changes to take     effect, though the app does poll every 2 seconds and update            automatically. MetaMask should fix the issue soon.
 * To acquire Kovan ethers, use [`this github faucet`]  (https://github.com/kovan-testnet/faucet#github-gist-faucet--automated-)
 * Withdraw functionality does not work. It signs a MetaMask transaction, and that's all. I'm still looking for the best way to do this, probably with Zero Client Provider


## Author's Note:
This was my first time working with Elixir/Phoenix and it's awesome.  It's also my first time creating a front-end website so my code might be not so awesome.  This MetaMask project part of something bigger I'm releasing soon, a esports peer-to-peer betting site (with no ICO!) called ethduel.com, and I'm making a longer writeup on the pitfalls and triumphs I found while working with Elixir/Phoenix/MetaMask/Web3, and here's a thank you to the creators of those!

Lil' preview:
![](/assets/static/images/gamescreen10102017.png)


## Learn more about Phoenix

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
